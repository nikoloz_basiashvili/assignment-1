import kotlin.test.assertEquals

fun main(){
    // #1 Avarage of even indexed numbers tests
    assertEquals(evenIndexAvg(arrayOf(1,2,3,4,5,6)), 3.0)
    assertEquals(evenIndexAvg(arrayOf(1)), 1.0)
    assertEquals(evenIndexAvg(arrayOf()), 0.0)
    assertEquals(evenIndexAvg(arrayOf(-1, 2, 3, 100, -2)), 0.0)
    assertEquals(evenIndexAvg(arrayOf(1,2,3,4,5,6)), 3.0)
    //This test was absolutely calculated beforehand on paper by me
    assertEquals(evenIndexAvg(arrayOf(29391, 12312, 427184, 12227, 97271, -23712, 214719, 214921, -5357, 21302, 123814, 912988)), 147837.0)

    // #2 is palindrome tests
    assertEquals(isPalindrome("civic"), true);
    assertEquals(isPalindrome("deified"), true);
    assertEquals(isPalindrome("aaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbaaaaaaaaaaaa"), true);
    assertEquals(isPalindrome("nika"), false);
    assertEquals(isPalindrome("ananas"), false);
}

//Returns average of numbers on even index.
fun evenIndexAvg(arr : Array<Int>) : Double{
    var sum : Int = 0;
    var len : Int = 0;
    for(i:Int in 0 until arr.size step 2){
        sum += arr[i];
        len++;
    }
    if(len == 0)
        return 0.0
    return sum.toDouble()/len
}

//Returns if word is a palindrome or not
fun isPalindrome(str : String) : Boolean{
    val len = str.length
    for(i in 0 until len){
        if(str[i] != str[len - i - 1])
            return false
    }
    return true
}